# Docker setup for [cubicweb-saem-ref][]

The Docker setup consists of three services based on a single docker image.
Services are managed with docker-compose.

To build the base image, run:

    make build

to specify a particular version, override the `VERSION` variable, e.g. `make
build VERSION=0.25.6`.

Alternatively, the image can be pulled from hub.docker.com registry as:

    docker pull logilab/saemref

(or `docker pull logilab/saemref:1.0.0` to pull a specific version).

To start the services, simply run:

    docker-compose up [-d]

The `default.env` file provide default values for most environment variables.
It is often useful to override some of them and add some more. Especially, it
is advised to add the following extra variables in a local `.env` file:

    PYRAMID_SESSION_SECRET=<secret 1>
    PYRAMID_AUTHTKT_SESSION_SECRET=<secret 2>
    PYRAMID_AUTHTKT_PERSISTENT_SECRET=<secret 3>

filled with actual secret strings.

If working on a fresh instance (i.e. without an existing database with actual
data), it is needed to first create and initialize the database. This can be
done as follows:

    docker-compose run web db-create

This commands will only work if the PostgreSQL user has permissions to
actually create the database in the cluster. If the database already exists
(created by a superuser), the following command will only initialize the
database:

    docker-compose run web db-create -cn --automatic

To run a database migration, e.g. when a new version of the base image is
available, run:

    docker-compose run web upgrade

and anwser the usual question from `cubicweb-ctl upgrade`. Alternatively, it
is possible to pass options to the command like:

    docker-compose run web upgrade --backup-db=no --force --verbosity=0

To execute maintenance commands inside a Docker container, use:

    docker-compose run --rm web cubicweb-ctl <subcommand> instance <args>

For instance, to synchronize a data source name "matiere", we'd call the
`source-sync` command as:

    docker-compose run --rm cubicweb-ctl source-sync instance matiere

Some commands may need files from the local file system; in order to make
these available from within the container, volumes can be mounted using `-v`
option of `docker-compose run`.

[cubicweb-saem-ref]: https://cubicweb-saem-ref.readthedocs.io/fr/latest/
