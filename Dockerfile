FROM debian:buster-slim
ENV LANG C.UTF-8
RUN mkdir -p /usr/share/man/man1 && mkdir -p /usr/share/man/man7
RUN apt-get update && apt-get -y dist-upgrade \
    && rm -rf /var/lib/apt/lists/*
RUN apt-get update && apt-get -y --no-install-recommends install \
    gettext \
    uwsgi \
    uwsgi-plugin-python3 \
    graphviz \
    postgresql-client \
    python3-pip \
    python3-setuptools \
    python3-crypto \
    python3-psycopg2 \
    python3-jinja2 \
    pwgen \
    && rm -rf /var/lib/apt/lists/*
RUN update-alternatives --install /usr/bin/python python /usr/bin/python3 50 && \
    update-alternatives --install /usr/bin/pip pip /usr/bin/pip3 50
ENV PIP_NO_CACHE_DIR off
ENV PIP_DISABLE_PIP_VERSION_CHECK on
# Markdown require setuptools>=36
RUN pip install 'setuptools>=36'
ARG version
RUN pip install cubicweb-saem-ref==${version}
RUN useradd cubicweb --uid 1000 -m -s /bin/bash
RUN install -d -o cubicweb -g cubicweb /etc/cubicweb.d
COPY uwsgi.ini /etc/uwsgi/uwsgi.ini
RUN echo "plugins = http,python3" >> /etc/uwsgi/uwsgi.ini
COPY pyramid.ini.j2 /
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENV CW_INSTANCE=instance
ENV CW_INSTANCES_DIR=/etc/cubicweb.d
ENV CW_INSTANCES_DATA_DIR=/etc/cubicweb.d
ENV CW_LOG_FILE=/dev/stdout
ENV CW_LOG_THRESHOLD=WARNING
ENV CW_DB_HOST=
ENV CW_DB_USER=cubicweb
ENV CW_DB_PASSWORD=
ENV CW_DB_DRIVER=postgres
ENV CW_LOGIN=admin
ENV CW_PASSWORD=admin
ENV CW_BASE_URL=http://localhost:8080
USER cubicweb
WORKDIR /home/cubicweb
RUN cubicweb-ctl create saem_ref $CW_INSTANCE --automatic --no-db-create
EXPOSE 8080/tcp
ENTRYPOINT ["/entrypoint.sh"]
CMD ["start"]
