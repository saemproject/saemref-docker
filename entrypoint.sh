#!/bin/sh
set -e
CUBE=saem_ref
export CUBE

if test -z "$CW_DB_NAME"; then
    export CW_DB_NAME=$CUBE
fi
PINI=$CW_INSTANCES_DIR/$CW_INSTANCE/pyramid.ini
if ! test -e "$PINI"; then
    test -z "$PYRAMID_SESSION_SECRET" && export PYRAMID_SESSION_SECRET=$(pwgen -s 20)
    test -z "$PYRAMID_AUTHTKT_SESSION_SECRET" && export PYRAMID_AUTHTKT_SESSION_SECRET=$(pwgen -s 20)
    test -z "$PYRAMID_AUTHTKT_PERSISTENT_SECRET" && export PYRAMID_AUTHTKT_PERSISTENT_SECRET=$(pwgen -s 20)
    python > $PINI << EOF
import os
import jinja2
print(jinja2.Template(open('/pyramid.ini.j2').read()).render(os.environ))
EOF
fi

db_create() {
    echo "creating instance database"
    if [ "$#" -lt 1 ]
    then
        options="--automatic"
    else
        options="$*"
    fi
    cubicweb-ctl db-create "$options" "$CW_INSTANCE"
}

upgrade() {
    echo "upgrading instance"
    cubicweb-ctl upgrade "$@" "$CW_INSTANCE"
    cubicweb-ctl gen-static-datadir "$CW_INSTANCE"
    cubicweb-ctl i18ninstance "$CW_INSTANCE"
}

case "$1" in
    start)
        exec uwsgi --ini /etc/uwsgi/uwsgi.ini
        ;;
    upgrade)
        shift
        upgrade "$@"
        ;;
    db-create)
        shift
        db_create "$@"
        ;;
    -h|--help|help)
        cat << EOF
Docker image for cubicweb-$CUBE

Usage:

start                   start instance
db-create [OPTIONS]     initialize database
upgrade [OPTIONS]       upgrade instance (extra options are passed to
                        cubicweb-ctl upgrade <instance>)


Environment variables:

CW_BASE_URL                             site url (default http://localhost:8080)
CW_LOG_THRESHOLD                        log level (default WARNING)
CW_DB_HOST                              database host
CW_DB_USER                              database user (default cubicweb)
CW_DB_PASSWORD                          database password
CW_DB_NAME                              database name (default $CUBE)
CW_DB_DRIVER                            database driver (default postgres)
PYRAMID_SESSION_SECRET
PYRAMID_AUTHTKT_SESSION_SECRET
PYRAMID_AUTHTKT_PERSISTENT_SECRET
EOF
        exit 64
        ;;
    *)
        exec "$@"
        ;;
esac
