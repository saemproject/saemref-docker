VERSION?=1.0.0

build:
	docker build \
		--build-arg version=$(VERSION) \
		--tag logilab/saemref:$(VERSION) \
		.

latest: build
	docker tag logilab/saemref:${VERSION} logilab/saemref:latest

publish: latest build
	docker push logilab/saemref:$(VERSION)
	docker push logilab/saemref:latest
